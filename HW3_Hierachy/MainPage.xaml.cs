﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW3_Hierachy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private int state = 0; 
        private string p1 = "";
        //private string p2 = "";
        public MainPage()
        {
            InitializeComponent();

        }

        async void OnRock(object sender, EventArgs args)
        {
            if(state == 0) p1 = "rock";
            await Navigation.PushAsync(new Page1(state, p1,pSel));
        }

        async void OnPaper(object sender, EventArgs args)
        {
            if (state == 0) p1 = "paper";
            await Navigation.PushAsync(new Page2(state, p1, pSel));
        }

        async void OnScissors(object sender, EventArgs args)
        {
            if (state == 0) p1 = "scissors";
            await Navigation.PushAsync(new Page3(state, p1, pSel));
        }

        void OnReset(object sender, EventArgs args)
        {
            state = 0;
            gameState.Text = "P1";
            pSel.Text = "";

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (state == 0) gameState.Text = "P1";
            else if (state == 1) gameState.Text = "P2";
            
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (state < 2) state++;

        }

    }
}
