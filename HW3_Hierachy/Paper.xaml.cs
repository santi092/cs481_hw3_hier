﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Hierachy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        private int gState;
        private string P1;
        public Page2(int state, string p1, Label sel)
        {
            InitializeComponent();
            gState = state;
            P1 = p1;
            pSel = sel;
        }

        public Label pSel { get; }
    async void OnScore(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new Score());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (gState == 0) pState.Text = "Player 1 selects Paper";
            else if (gState == 1) pState.Text = "Player 2 selects Paper";
            else pState.Text = "Error";

        }

        protected override void OnDisappearing()
        {
            if (gState == 0) DisplayAlert("Notice", "Player 1 selected", "OK");
            else if (gState == 1)
            {
                if (P1 == "rock") DisplayAlert("Notice","Paper beats rock: Player 2 wins","OK");
                else if (P1 == "scissors") DisplayAlert("Notice","Scissors beats paper: Player 1 wins", "OK");
                else if (P1 == "paper") DisplayAlert("Notice", "Tie", "OK");
            }
            //pSel.Text = "Selected Paper";
        }
        
    }
}