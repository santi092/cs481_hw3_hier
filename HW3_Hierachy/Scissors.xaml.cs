﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HW3_Hierachy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {

        private int gState;
        private string P1;
        
        public Page3(int state,string p1, Label sel)
        {
            InitializeComponent();
            gState = state;
            P1 = p1;
            pSel = sel;
        }

        public Label pSel { get; }
        async void OnScore(object sender, EventArgs args)
        {

            await Navigation.PushAsync(new Score());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (gState == 0) sState.Text = "Player 1 selects Scissors";
            else if (gState == 1) sState.Text = "Player 2 selects Scissors";
            else sState.Text = "Error";

        }

        protected override void OnDisappearing()
        {
            if (gState == 0) DisplayAlert("Notice","Player 1 selected","OK");
            else if (gState == 1)
            {
                if (P1 == "rock") DisplayAlert("Notice","Rock beats scissors: Player 1 wins","OK");
                else if (P1 == "paper") DisplayAlert("Notice","Scissors beats paper: Player 2 wins","OK");
                else if (P1 == "scissors") DisplayAlert("Notice", "Tie", "OK");
            }
            //pSel.Text = "Selected Scissors";
        }
    }
}